//Example 1: defining odd numbers
function oddNum(min, max) {
    let myNum = [];
    for (let i = min; i <= max; i++) {
        if (i%2 !== 0) {
            myNum.push(i);
        }
    }
    return myNum;
}
console.log(oddNum(1, 9)); // return [ 1, 3, 5, 7, 9 ]


// Example 2: summing array elements
// try using ES6
const sum = arr => {
    let addition = 0;
    let i = 0;
    while (i < arr.length) {
        addition += arr[i];
        i++;
    }
    return addition;
}

console.log(sum([3, 2, 1, 2, 3])) // return 11

// Example 3: looping through all array elements in reverse order
let myArr = ["go!", "one", "two", "three", "four", "five", "here we go!", "reverse order"];
for (let i = myArr.length - 1; i >= 0; i--) {
    console.log(myArr[i]);
}

function multi() {


}

/* 7. Write a JavaScript program to construct the following pattern,
using a nested for loop. */
let a = " ";
for (let i = 1; i <= 6; i++){
  for (let j = 1; j <= i; j++) {
    a = a + ("*");
  }
  document.write("<br>" + a);
  a = ''; 
}

/*
*
**
***
****
*****
****** */

// Complete the staircase function below.
function staircase(n) {
    // looping for spaces
      for(let i = 0; i < n; i++){
          // in every looping, loop through the spaces / empty string
          let output = '';
          for(let j = 0; j < n; j++){
            // Loop through, whenever (n-1-i) is bigger than j concat a space else #
            if (j < (n -1 -i)) { // ex : 6 - 1 - 0 = 5 spaces in the first loop
                output += ' ';        // 6 - 1 - 1 = 4 spaces in the second loop
            } else {                  // 6 - 1 - 2 = 3 spaces in the third loop, etc
                output += '*';
            }
          }
          console.log(output);
      }
  }

  staircase(6);


//  Basic JavaScript: Record Collection
//   You are given a JSON object representing a part of your musical album collection. Each album has a unique id number as its key and several other properties. Not all albums have complete information.
//   You start with an updateRecords function that takes an object like collection, an id, a prop (like artist or tracks), and a value. Complete the function using the rules below to modify the object passed to the function.
  
//   Your function must always return the entire object.
//   If prop isn't tracks and value isn't an empty string, update or set that album's prop to value.
//   If prop is tracks but the album doesn't have a tracks property, create an empty array and add value to it.
//   If prop is tracks and value isn't an empty string, add value to the end of the album's existing tracks array.
//   If value is an empty string, delete the given prop property from the album.
//   Note: A copy of the collection object is used for the tests. 

// Setup
var collection = {
    2548: {
      albumTitle: 'Slippery When Wet',
      artist: 'Bon Jovi',
      tracks: ['Let It Rock', 'You Give Love a Bad Name']
    },
    2468: {
      albumTitle: '1999',
      artist: 'Prince',
      tracks: ['1999', 'Little Red Corvette']
    },
    1245: {
      artist: 'Robert Palmer',
      tracks: []
    },
    5439: {
      albumTitle: 'ABBA Gold'
    }
  };
  

  // Only change code below this line
  function updateRecords(object, id, prop, value) {
    if (prop !== "tracks" && value !== "") {
        object[id][prop] = value;
    } else if (prop === "tracks" && !object[id].hasOwnProperty("tracks")) {
        object[id][prop] = [value];
    } else if (prop === "tracks" && value !== "") {
        object[id][prop].push(value);
    } else if (value === "") {
        delete object[id][prop];
    }
    return object;
  }
  // console.log(updateRecords(collection, 5439, 'artist', 'ABBA'));
  // console.log(updateRecords(collection, 5439, "tracks", "Take a Chance on Me"));
  // console.log(updateRecords(collection, 2548, "artist", ""))
  // console.log(updateRecords(collection, 1245, "tracks", "Addicted to Love"));
  // console.log(updateRecords(collection, 2468, "tracks", "Free"));
  // console.log(updateRecords(collection, 2548, "tracks", ""));
  // console.log(updateRecords(collection, 1245, "albumTitle", "Riptide"));

  // or try this solution 
  // Only change code below this line
    function updateRecords(object, id, prop, value) {
        if (value === '') delete object[id][prop];
        else if (prop === 'tracks') {
            object[id][prop] = object[id][prop] || []; // this is called shortcircuit evaluation, see below for explanation
            object[id][prop].push(value);
        } else {
            object[id][prop] = value;
        }

        return object;
    }


    //RECURSION
function countdown(n){
    return n < 1 ? [] : [n].concat(countdown(n - 1));
  }
  
  console.log(countdown(5)) // RETURN [ 5, 4, 3, 2, 1 ]