// Only change code below this line
class Thermostat {
    constructor(fahrenheit) {
        this.temperature = 5/9 * (fahrenheit - 32); // in C
    }

    get tempC() {
        return this.temperature;
    }

    set tempF(celcius) {
        this.temperature = celcius * 9 / 5 + 32; // in F
    }
}

// Only change code above this line

const suhu = new Thermostat(76);
console.log(suhu.tempC); // 24.444 C
suhu.tempF = 24.44446;
console.log(suhu.tempC);

class Book {
    constructor(author) {
        this._auth = author;
    }
    get writer() {
        return this._auth;
    }
    set writer1(newAuthor) {
        this._auth = newAuthor
    }
}


const novel = new Book ("Jim");
console.log(novel.writer);
novel.writer1 = "Kimy";
console.log(novel.writer)